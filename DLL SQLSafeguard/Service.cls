VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Service"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Public TmpCode As String, TmpTitle As String, TmpKey As String

Public Function RequestAccess(ByVal Code As Long, ByVal Title As String, ByVal Key As String) As Variant
    Initialize
    TmpCode = Code: TmpTitle = Title: TmpKey = Key
    Set FrmAccess.fService = Me
    FrmAccess.Show vbModal
    RequestAccess = FrmAccess.Credentials
    Set FrmAccess = Nothing
End Function

Private Function GetPassKey(ByVal Code As Long, ByVal Title As String, ByVal Key As String) As String: GetPassKey = (CStr(Code) & "|" & Title & "|" & Key): End Function

Public Function Encode(ByVal Code As Long, ByVal Title As String, ByVal Key As String, ByVal pValue As String) As String: On Error Resume Next: Encode = ToHexDump(CryptRC4(pValue, GetPassKey(Code, Title, Key))): End Function

Public Function Decode(ByVal Code As Long, ByVal Title As String, ByVal Key As String, ByVal pValue As String) As String: On Error Resume Next: Decode = CryptRC4(FromHexDump(pValue), GetPassKey(Code, Title, Key)): End Function

Private Function CryptRC4(sText As String, sKey As String) As String
    Dim baS(0 To 255) As Byte
    Dim baK(0 To 255) As Byte
    Dim bytSwap     As Byte
    Dim lI          As Long
    Dim lJ          As Long
    Dim lIdx        As Long

    For lIdx = 0 To 255
        baS(lIdx) = lIdx
        baK(lIdx) = Asc(Mid$(sKey, 1 + (lIdx Mod Len(sKey)), 1))
    Next
    For lI = 0 To 255
        lJ = (lJ + baS(lI) + baK(lI)) Mod 256
        bytSwap = baS(lI)
        baS(lI) = baS(lJ)
        baS(lJ) = bytSwap
    Next
    lI = 0
    lJ = 0
    For lIdx = 1 To Len(sText)
        lI = (lI + 1) Mod 256
        lJ = (lJ + baS(lI)) Mod 256
        bytSwap = baS(lI)
        baS(lI) = baS(lJ)
        baS(lJ) = bytSwap
        CryptRC4 = CryptRC4 & Chr$((pvCryptXor(baS((CLng(baS(lI)) + baS(lJ)) Mod 256), Asc(Mid$(sText, lIdx, 1)))))
    Next
End Function

Private Function pvCryptXor(ByVal lI As Long, ByVal lJ As Long) As Long
    If lI = lJ Then
        pvCryptXor = lJ
    Else
        pvCryptXor = lI Xor lJ
    End If
End Function

Private Function ToHexDump(sText As String) As String
    Dim lIdx            As Long

    For lIdx = 1 To Len(sText)
        ToHexDump = ToHexDump & Right$("0" & Hex(Asc(Mid(sText, lIdx, 1))), 2)
    Next
End Function

Private Function FromHexDump(sText As String) As String
    Dim lIdx            As Long

    For lIdx = 1 To Len(sText) Step 2
        FromHexDump = FromHexDump & Chr$(CLng("&H" & Mid(sText, lIdx, 2)))
    Next
End Function
