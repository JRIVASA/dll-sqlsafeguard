VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form FrmAccess 
   BackColor       =   &H009E5300&
   BorderStyle     =   0  'None
   ClientHeight    =   4965
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   7140
   LinkTopic       =   "Form1"
   ScaleHeight     =   4965
   ScaleWidth      =   7140
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox TxtTitle 
      Alignment       =   2  'Center
      BackColor       =   &H009E5300&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000F&
      Height          =   735
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   8
      TabStop         =   0   'False
      Text            =   "FrmEnterValues.frx":0000
      Top             =   120
      Width           =   6855
   End
   Begin VB.CheckBox CmdCancel 
      BackColor       =   &H009E5300&
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000F&
      Height          =   585
      Left            =   4560
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   3960
      Width           =   1335
   End
   Begin VB.TextBox TxtPassWord 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      IMEMode         =   3  'DISABLE
      Left            =   2880
      PasswordChar    =   "*"
      ScrollBars      =   2  'Vertical
      TabIndex        =   1
      Text            =   "PRUEBA"
      Top             =   3015
      Width           =   2925
   End
   Begin VB.TextBox TxtUsuario 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      IMEMode         =   3  'DISABLE
      Left            =   2880
      PasswordChar    =   "*"
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Text            =   "sssssss"
      Top             =   1680
      Width           =   2925
   End
   Begin VB.CheckBox CmdEnter 
      BackColor       =   &H009E5300&
      Caption         =   "Enter"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000F&
      Height          =   585
      Left            =   2880
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   3960
      Width           =   1335
   End
   Begin MSFlexGridLib.MSFlexGrid GridEvitarFoco 
      Height          =   30
      Left            =   5400
      TabIndex        =   7
      Top             =   1800
      Width           =   30
      _ExtentX        =   53
      _ExtentY        =   53
      _Version        =   393216
   End
   Begin VB.Label lblPass 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Password:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   2910
      TabIndex        =   6
      Top             =   2460
      Width           =   1095
   End
   Begin VB.Label lblUser 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Username:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   2910
      TabIndex        =   5
      Top             =   1200
      Width           =   1155
   End
   Begin VB.Image CmdTeclado 
      Height          =   600
      Left            =   6285
      MousePointer    =   99  'Custom
      Picture         =   "FrmEnterValues.frx":0054
      Stretch         =   -1  'True
      Top             =   1590
      Width           =   480
   End
   Begin VB.Image ImgBackground 
      Height          =   1800
      Left            =   480
      Picture         =   "FrmEnterValues.frx":1DD6
      Top             =   1200
      Width           =   1800
   End
   Begin VB.Label lblAuth 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Authentication"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   735
      Left            =   120
      TabIndex        =   4
      Top             =   3015
      Width           =   2655
   End
End
Attribute VB_Name = "FrmAccess"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Credentials As Variant
Public fService As SQLSafeGuard.Service

Private Sub CmdCancel_Click()
    'If CmdCancel.Value = vbChecked Then
        Credentials = Empty
        Unload Me
        'If PuedeObtenerFoco(GridEvitarFoco) Then GridEvitarFoco.SetFocus
    'End If
End Sub

Private Sub CmdEnter_Click()
    'If CmdEnter.Value = vbChecked Then
        With fService
            Credentials = Array(TxtUsuario.Text, TxtPassWord.Text, _
            .Encode(.TmpCode, .TmpTitle, .TmpKey, TxtUsuario.Text), _
            .Encode(.TmpCode, .TmpTitle, .TmpKey, TxtPassWord.Text))
        End With
        Unload Me
        'If PuedeObtenerFoco(GridEvitarFoco) Then GridEvitarFoco.SetFocus
    'End If
End Sub

Private Sub CmdTeclado_Click()
    
    Dim mCtl As Object
    
    If Not Me.ActiveControl Is Nothing Then
        If TypeOf Me.ActiveControl Is TextBox Then
            Set mCtl = Me.ActiveControl
        End If
    End If
    
    If mCtl Is Nothing Then Set mCtl = TxtUsuario
    
    TecladoWindows mCtl
    
End Sub

Private Sub Form_Load()
    ' System Authentication.
    Me.TxtUsuario.Text = vbNullString
    Me.TxtPassWord.Text = vbNullString
    Me.TxtTitle.Text = Replace(StellarMensaje(9), "$(Line)", vbNewLine)
    Me.lblAuth.Caption = StellarMensaje(1) ' Authentication
    Me.lblUser.Caption = StellarMensaje(2) ' Username
    Me.lblPass.Caption = StellarMensaje(3) ' Password
    Me.CmdEnter.Caption = StellarMensaje(5) ' Enter
    Me.CmdCancel.Caption = StellarMensaje(6) ' Cancel
End Sub

Private Sub TxtPassWord_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        CmdEnter_Click
    End If
End Sub

Private Sub TxtUsuario_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        If PuedeObtenerFoco(TxtPassWord) Then TxtPassWord.SetFocus
    End If
End Sub
